const fs = require('fs');
const chalk = require('chalk');

const getNotes = ()=>{
    console.log('Get Notes..')
}


const addNote = (title, body)=>{
    const data = loadNotes();
    data.push({
        title:title,
        body:body
    })
    fs.writeFileSync('notes.txt',JSON.stringify(data));
}

const loadNotes = ()=>{
    try{
        const bufferData = fs.readFileSync('notes.txt');
        const stringData = bufferData.toString();
        return JSON.parse(stringData);
    }catch(e){
        return []
    }    
}

const removeNotes = (title)=>{
    const data = loadNotes();
    const newdata = data.filter((i)=>i.title !==title)
    debugger
    fs.writeFileSync('notes.txt',JSON.stringify(newdata));
    if (data.length==newdata.length){
        console.log(chalk.red.inverse('No note found'))
    }else{
        console.log(chalk.green.inverse('Note deleted'))
    }
}

module.exports = {
    getNotes:loadNotes,
    addNote:addNote,
    removeNotes:removeNotes
}