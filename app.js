// // const fs = require('fs');
// // // fs.writeFileSync('notes.txt','This file was created by Node.js!');
// // fs.appendFileSync('notes.txt','Add new data from Node !');
const notes = require('./notes.js');
const yargs = require("yargs")

// const validator = require('validator');
// console.log(validator.isEmail('@ab.com'));

 const chalk = require('chalk');
// console.log(chalk.green('success'))

// console.log(process.argv)



yargs.command({
    command:'add',
    describe:'to add numbers',
    builder:{
        title:{
            describe:'Notes title',
            demandOption:true,
            type:'string'
        },
        body:{
            describe:'Notes Body',
            demandOption:true,
            type:'string'
        }
    },
    handler(argv){
        notes.addNote(argv.title,argv.body);
        console.log(chalk.green.inverse('note added'));
    }
});

yargs.command({
    command:'remove',
    describe:'remove content',
    builder:{
        title:{
            describe:'Notes title',
            demandOption:true,
            type:'string'
        }
    },
    handler(argv){
        console.log(notes.removeNotes(argv.title));
    }
})

yargs.command({
    command:'read',
    describe:'read data',
    handler(){
        console.log(notes.getNotes());
    }
})

yargs.command({
    command:'list',
    describe:'list all notes',
    handler(){
        console.log('list hte notes here');
    }
})

// console.log(yargs.argv)
yargs.parse();